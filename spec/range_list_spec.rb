require 'range_list'

RSpec.describe RangeList, 'Range List Integration Test' do
  before :all do
    @range_list = RangeList.new
  end

  def range_list
    @range_list
  end

  context " Methods of 'add', 'remove' and 'print' continuous called " do
    it 'add([1, 5]), Should display: [1, 5)' do
      range_list.add([1, 5])
      expect(range_list.print).to eq('[1, 5)')
    end

    it 'add([10, 20]), Should display: [1, 5) [10, 20)' do
      range_list.add([10, 20])
      expect(range_list.print).to eq('[1, 5) [10, 20)')
    end

    it 'add([20, 20]), Should display: [1, 5) [10, 20)' do
      range_list.add([20, 20])
      expect(range_list.print).to eq('[1, 5) [10, 20)')
    end

    it 'add([20, 21]), Should display: [1, 5) [10, 21)' do
      range_list.add([20, 21])
      expect(range_list.print).to eq('[1, 5) [10, 21)')
    end

    it 'add([2, 4]), Should display: [1, 5) [10, 21)' do
      range_list.add([2, 4])
      expect(range_list.print).to eq('[1, 5) [10, 21)')
    end

    it 'add([3, 8]), Should display: [1, 8) [10, 21)' do
      range_list.add([3, 8])
      expect(range_list.print).to eq('[1, 8) [10, 21)')
    end

    it 'remove([10, 10]), Should display: [1, 8) [10, 21)' do
      range_list.remove([10, 10])
      expect(range_list.print).to eq('[1, 8) [10, 21)')
    end

    it 'remove([10, 11]), Should display: [1, 8) [11, 21)' do
      range_list.remove([10, 11])
      expect(range_list.print).to eq('[1, 8) [11, 21)')
    end

    it 'remove([15, 17]), Should display: [1, 8) [11, 15) [17, 21)' do
      range_list.remove([15, 17])
      expect(range_list.print).to eq('[1, 8) [11, 15) [17, 21)')
    end

    it 'remove([3, 19]), Should display: [1, 3) [19, 21)' do
      range_list.remove([3, 19])
      expect(range_list.print).to eq('[1, 3) [19, 21)')
    end

  end
end
