class RangeList
  attr_accessor :list

  def initialize
    self.list = []
  end

  def add(range)
    # 并集
    self.list = (self.list | range.elements).sort
  end

  def remove(range)
    # 差集
    self.list = (self.list - range.elements).sort
  end

  def print
    a = self.list
    prev = a[0]
    a.slice_before { |e|
      prev, prev2 = e, prev
      prev2 + 1 != e
    }.map { |es|
       "[#{es.first}, #{es.last + 1})"
    }.join(" ")
  end

end

class Array
  # [1, 3] => (1...3)
  def elements
    raise "Range is not valid" unless self.length == 2 && self[0].class == Integer && self[1].class == Integer && self[0] <= self[1]
    (self[0]...self[1]).to_a
  end

end


